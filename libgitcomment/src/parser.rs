extern crate combine;

use self::combine::*;
use self::combine::primitives::Stream;

/// Parse a Person from a string in the format:
///
/// `[name] <[email]> [date]`
pub fn person<I>(input: State<I>) -> ParseResult<super::Person, I>
    where I: Stream<Item = char> {
    // match name by iterating over all characters until a '<' or newline.
    // a space would never be matched afterward since it can appear in names
    // and the parser is greedy
    (many::<String, _>(satisfy(|c| c != '<' && c != '\n')),
     // parse email address between '<' and '>'
     between(token('<'), token('>'),
             many1::<String, _>(satisfy(|c| c != '>' && c != '\n'))),
     // require a single space before the date
     token(' '),
     // parse person date
     parser(date))
        .map(|(name, email, _, date)| {
            super::Person {
                name: name.trim().to_string(),
                email: email,
                date: date
            }
        }).parse_state(input)
}

/// Parse a Date from a string in the format:
///
/// `[timestamp] [timezone offset]`
pub fn date<I>(input: State<I>) -> ParseResult<super::Date, I>
    where I: Stream<Item = char> {
     // parse some number of digits as the timestamp
     (many1::<String, _>(digit()),
      // require a single space
      token(' '),
      // parse a positive or negative timezone offset in the format +HHMM or
      // -HHMM
      many1::<String, _>(satisfy(|c| c == '-' || c == '+').or(digit())))
         .map(|(timestamp_str, _, timezone)| {
            // marshal timestamp into a number
            let timestamp = usize::from_str_radix(&timestamp_str, 10);
            super::Date {
                timestamp: timestamp.ok().unwrap(),
                timezone: timezone
            }
         }).parse_state(input)
}

/// Parse a File from a string in the format:
///
/// `[path]:[line]`
pub fn file<I>(input: State<I>) -> ParseResult<super::File, I>
    where I: Stream<Item = char> {
    // parse any character except colon as the path, then a single colon
    (many::<String, _>(satisfy(|c| c != ':')), token(':'),
     // require at least one digit as the line number
     many1::<String, _>(digit()))
        .map(|(path, _, line_str)| {
            // marshal the line number from a string
            let line = usize::from_str_radix(&line_str, 10);
            super::File { path: path, line: line.ok().unwrap() }
        }).parse_state(input)
}

/// Parse a Comment from a string
pub fn comment<I>(input: State<I>) -> ParseResult<super::Comment, I>
    where I: Stream<Item = char> {
    (parser(keyed_commit), parser(keyed_file),
     parser(keyed_author), parser(keyed_amender),
     newline(),
     parser(content))
        .map(|(commit, file, author, amender, _, text)| {
            super::Comment {
                commit: commit,
                file: file,
                author: author,
                amender: amender,
                content: text
            }
        }).parse_state(input)
}

/// Parse a line of text until a newline or end of input
fn text_value<I>(input: State<I>) -> ParseResult<String, I>
    where I: Stream<Item = char> {
     (many1::<String, _>(hex_digit()))
        .parse_state(input)
}

/// Parse multiline content as plain text until end of input
fn content<I>(input: State<I>) -> ParseResult<String, I>
    where I: Stream<Item = char> {
    many1(any()).expected("content").parse_state(input)
}

/// Parse a key and value in the format:
///
/// commit [hexdigits]
fn keyed_commit<I>(input: State<I>) -> ParseResult<String, I>
    where I: Stream<Item = char> {
    (string("commit"), token(' '), parser(text_value), optional(newline()))
        .map(|(_, _, commit, _)| commit)
        .expected("commit")
        .parse_state(input)
}

/// Parse a key and value in the format:
///
/// `file [file]`
fn keyed_file<I>(input: State<I>) -> ParseResult<Option<super::File>, I>
    where I: Stream<Item = char> {
     (string("file"), token(' '), optional(parser(file)), optional(newline()))
        .map(|(_, _, file, _)| file)
        .expected("file")
        .parse_state(input)
}

/// Parse a key and value in the format:
///
/// `author [person]`
fn keyed_author<I>(input: State<I>) -> ParseResult<super::Person, I>
    where I: Stream<Item = char> {
    (string("author"), token(' '), parser(person), optional(newline()))
        .map(|(_, _, person, _)| person)
        .expected("author")
        .parse_state(input)
}

/// Parse a key and value in the format:
///
/// `amender [person]`
fn keyed_amender<I>(input: State<I>) -> ParseResult<super::Person, I>
    where I: Stream<Item = char> {
    (string("amender"), token(' '), parser(person), optional(newline()))
        .map(|(_, _, person, _)| person)
        .expected("amender")
        .parse_state(input)
}

#[test]
fn test_person() {
    let result = parser(person).parse("Katie Doe <katie@example.com> 1243040974 -0900");
    let person = super::Person {
        name: "Katie Doe".to_string(),
        email: "katie@example.com".to_string(),
        date: super::Date {
            timestamp: 1243040974,
            timezone: "-0900".to_string()
        }
    };
    assert_eq!(result, Ok((person, "")));
}

#[test]
fn test_file() {
    let result = parser(file).parse("src/parser.c:37");
    let path = super::File { path: "src/parser.c".to_string(), line: 37 };
    assert_eq!(result, Ok((path, "")));
}

#[test]
fn test_comment() {
    let content = r#"commit 0155eb4229851634a0f03eb265b69f5a2d56f341
file src/parser.c:12
author Daniel Pri <dan@example.com> 1243040974 -0900
amender Daniel Pri <dan@example.com> 1243040974 -0900

Too many levels of indentation here.

Consider updating the linter as well as a part of this changeset."#;
    let result = parser(comment).parse(content);
    let comment = super::Comment {
        commit: "0155eb4229851634a0f03eb265b69f5a2d56f341".to_string(),
        file: Some(super::File {
            path: "src/parser.c".to_string(),
            line: 12
        }),
        author: super::Person {
            name: "Daniel Pri".to_string(),
            email: "dan@example.com".to_string(),
            date: super::Date {
                timestamp: 1243040974, timezone: "-0900".to_string()
            }
        },
        amender: super::Person {
            name: "Daniel Pri".to_string(),
            email: "dan@example.com".to_string(),
            date: super::Date {
                timestamp: 1243040974, timezone: "-0900".to_string()
            }
        },
        content: r#"Too many levels of indentation here.

Consider updating the linter as well as a part of this changeset."#.to_string()
    };
    assert_eq!(result, Ok((comment, "")));
}
