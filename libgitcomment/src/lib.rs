//! Parsing and representation for the structures in a git comment.
#![doc(html_logo_url = "http://git-comment.com/images/git-comment.svg")]

extern crate combine;

mod parser;

use combine::Parser;
use std::convert::{TryFrom,Into};

/// A comment on a commit in a git repository and related metadata
#[derive(PartialEq, Clone, Debug)]
pub struct Comment {
    /// Text content of the comment
    pub content: String,
    /// Full hash of the commit to which the comment is attached
    pub commit: String,
    /// File path and line number
    pub file: Option<File>,
    /// The writer of the comment
    pub author: Person,
    /// The committer of the comment
    pub amender: Person,
}

/// A file path and line number.
#[derive(PartialEq, Clone, Debug)]
pub struct File {
    /// File path relative to the root of the git repository
    pub path: String,
    /// Line number
    pub line: usize,
}

/// A person interacting with a comment, such as an author or committer.
#[derive(PartialEq, Clone, Debug)]
pub struct Person {
    /// The person's name
    pub name: String,
    /// The person's email
    pub email: String,
    /// The time of the interaction
    pub date: Date,
}

/// A date including a Unix timestamp and a timezone offset.
#[derive(PartialEq, Clone, Debug)]
pub struct Date {
    /// A Unix timestamp
    pub timestamp: usize,
    /// A timezone offset in the format `-HHMM` or `+HHMM`
    pub timezone: String,
}

/// `Comment` serializes the fields of metadata in a key-value format
/// (delimited by a space between key and value), then a single newline
/// followed by the comment content.
///
/// # Examples
///
/// Given a fully serialized comment as `content`:
///
/// ```text
/// commit 0155eb4229851634a0f03eb265b69f5a2d56f341
/// file src/main.c:12
/// author Daniel Pri <dan@example.com> 1243040974 -0900
/// amender Daniel Pri <dan@example.com> 1243040974 -0900
///
/// Too many levels of indentation here.
///
/// Consider updating the linter as well as a part of this changeset.
/// ```
///
/// A `Comment` can be deserialized from text:
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Comment;
/// # let content = "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\nfile src/parser.c:12\nauthor Daniel Pri <dan@example.com> 1243040974 -0900\namender Daniel Pri <dan@example.com> 1243040974 -0900\n\nToo many levels of indentation here.\n\nConsider updating the linter as well as a part of this changeset.";
/// let comment = Comment::try_from(content).unwrap();
/// let file = comment.file.unwrap();
/// assert_eq!(file.path, "src/parser.c");
/// assert_eq!(file.line, 12);
/// assert_eq!(comment.commit, "0155eb4229851634a0f03eb265b69f5a2d56f341");
/// assert_eq!(comment.author.name, "Daniel Pri");
/// assert_eq!(comment.author.email, "dan@example.com");
/// assert_eq!(comment.author.date.timestamp, 1243040974);
/// assert_eq!(comment.author.date.timezone, "-0900");
/// assert_eq!(comment.author, comment.amender);
/// assert_eq!(comment.content, "Too many levels of indentation here.\n\nConsider updating the linter as well as a part of this changeset.");
/// ```
///
/// # Failures
///
/// If any of the keywords are missing, an error results.
///
/// ```
/// # use libgitcomment::Comment;
/// # use std::convert::TryFrom;
/// assert!(Comment::try_from("").is_err());
/// ```
///
/// If a keyword is present but the value is not in the expected format, this
/// also results in an error.
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Comment;
/// assert!(Comment::try_from("commit hm").is_err());
/// ```
impl TryFrom<&str> for Comment {
    type Error = ();

    fn try_from(content: &str) -> Result<Self, Self::Error> {
        return match combine::parser(parser::comment).parse(content) {
            Ok(ok) => Ok(ok.0),
            Err(_) => Err(())
        }
    }
}

/// A `Comment` can be serialized as text suitable to deserialization using `TryFrom`
///
/// # Examples
///
/// ```
/// use std::convert::TryFrom;
/// # use libgitcomment::Comment;
/// # let content = "commit 0155eb4229851634a0f03eb265b69f5a2d56f341\nfile src/parser.c:12\nauthor Daniel Pri <dan@example.com> 1243040974 -0900\namender Daniel Pri <dan@example.com> 1243040974 -0900\n\nToo many levels of indentation here.\n\nConsider updating the linter as well as a part of this changeset.";
/// let comment = Comment::try_from(content).unwrap();
/// let serialized: String = comment.into();
/// assert_eq!(content, serialized);
/// ```
impl Into<String> for Comment {
    fn into(self) -> String {
        let file = if self.file.is_some() {
            self.file.clone().unwrap().into()
        } else {
            "".to_string()
        };
        let author: String = self.author.into();
        let amender: String = self.amender.into();
        return format!(r#"commit {}
file {}
author {}
amender {}

{}"#, self.commit, file, author, amender, self.content);
    }
}

/// `Date` uses the serialization format `timestamp timezone` where the
/// timezone is in the format `+HHMM` or `-HHMM`.
///
/// # Examples
///
/// Parsing a `Date` from text:
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Date;
/// let date = Date::try_from("1243040974 -0900").unwrap();
///
/// assert_eq!(date.timezone, "-0900");
/// assert_eq!(date.timestamp, 1243040974);
/// ```
///
/// # Failures
///
/// A number which cannot be converted into a Unix timestamp, a timezone
/// in an unrecognized format, or missing either component will result in
/// a parsing error.
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Date;
/// assert!(Date::try_from("-0900").is_err());
/// ```
///
/// Input which cannot be recognized as the correct format also returns an
/// error
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Date;
/// assert!(Date::try_from("Tomorrow").is_err());
/// ```
impl TryFrom<&str> for Date {
    type Error = ();

    fn try_from(content: &str) -> Result<Self, Self::Error> {
        return match combine::parser(parser::date).parse(content) {
            Ok(ok) => Ok(ok.0),
            Err(_) => Err(())
        }
    }
}

/// A `Date` can be serialized as text suitable to deserialization using `TryFrom`
///
/// # Examples
///
/// ```
/// # use libgitcomment::Date;
/// # use std::convert::TryFrom;
/// let date = Date::try_from("1243040974 -0900").unwrap();
/// let serialized: String = date.into();
/// assert_eq!(serialized, "1243040974 -0900");
/// ```
impl Into<String> for Date {
    fn into(self) -> String {
        return format!("{} {}", self.timestamp, self.timezone);
    }
}

/// `File` uses the serialization format `path:line`, where a colon is the
/// delimiter between the two components.
///
/// # Examples
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::File;
/// let file = File::try_from("src/parser.c:44").unwrap();
/// assert_eq!(file.path, "src/parser.c");
/// assert_eq!(file.line, 44);
/// ```
///
/// # Failures
///
/// Missing the colon or line number results in a parsing error
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::File;
/// assert!(File::try_from("src/parser.c").is_err());
/// assert!(File::try_from("src/parser.c:").is_err());
/// assert!(File::try_from("src/parser.c44").is_err());
/// ```
impl TryFrom<&str> for File {
    type Error = ();

    fn try_from(content: &str) -> Result<Self, Self::Error> {
        return match combine::parser(parser::file).parse(content) {
            Ok(ok) => Ok(ok.0),
            Err(_) => Err(())
        }
    }
}

/// A `File` can be serialized as text suitable to deserialization using `TryFrom`
///
/// # Examples
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::File;
/// let file = File::try_from("src/parser.c:44").unwrap();
/// let serialized: String = file.into();
/// assert_eq!("src/parser.c:44", serialized);
/// ```
impl Into<String> for File {
    fn into(self) -> String {
        return format!("{}:{}", self.path, self.line);
    }
}

/// `Person` uses the serialization format `name <email> date`, where `name` is
/// a person's name as it should be printed, `email` is their identifying email
/// address, and `date` is the time of comment interaction serialized as a
/// [`Date`](struct.Date.html).
///
/// # Examples
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Person;
/// let content = "Katie Doe <katie@example.com> 1243040974 -0900";
/// let person = Person::try_from(content).unwrap();
/// assert_eq!(person.name, "Katie Doe");
/// assert_eq!(person.email, "katie@example.com");
/// assert_eq!(person.date.timestamp, 1243040974);
/// assert_eq!(person.date.timezone, "-0900");
/// ```
///
/// # Failures
///
/// While a person's name can be empty, the email and date are required.
/// Missing a field results in a parsing error:
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Person;
/// assert!(Person::try_from("Katie Doe 1243040974 -0900").is_err());
///
/// let person = Person::try_from(" <katie@example.com> 1243040974 -0900");
/// assert_eq!(person.unwrap().name, "".to_string());
/// ```
impl TryFrom<&str> for Person {
    type Error = ();

    fn try_from(content: &str) -> Result<Self, Self::Error> {
        return match combine::parser(parser::person).parse(content) {
            Ok(ok) => Ok(ok.0),
            Err(_) => Err(())
        }
    }
}

/// A `Person` can be serialized as text suitable to deserialization using `TryFrom`
///
/// # Examples
///
/// ```
/// # use std::convert::TryFrom;
/// # use libgitcomment::Person;
/// let content = "Katie Doe <katie@example.com> 1243040974 -0900";
/// let person = Person::try_from(content).unwrap();
/// let serialized: String = person.into();
/// assert_eq!(content, serialized);
/// ```
impl Into<String> for Person {
    fn into(self) -> String {
        let date: String = self.date.into();
        return format!("{} <{}> {}", self.name, self.email, date);
    }
}
