
all: build

.PHONY: build

build: ## Build from source
	@cargo build

clean: ## Clean build artifacts
	@cargo clean

doc: ## Generate HTML documentation
	@cargo doc --no-deps --open

test: ## Run all tests
	@cargo test

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
